package my.net.xtend.genietemplate

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.TextInputLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView

class BaseTextInput : LinearLayout {

    companion object {
        private const val TEXT_VALUE = "edit_text_input_value"
        private const val INPUT_STATE = "edit_text_input_state"
        private const val SUPER_BUNDLE = "bundle_super_value"
    }

    val inputLayout: TextInputLayout
    val descTextView: TextView
    private var viewState: Boolean = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attr: AttributeSet?) : this(context, attr, 0)
    constructor(context: Context, attr: AttributeSet?, defStyleAttr: Int) : super(context, attr, defStyleAttr) {
        val typedArray = context.obtainStyledAttributes(attr, R.styleable.BaseTextInput, 0, 0)
        val textHint = typedArray.getString(R.styleable.BaseTextInput_hint)
        val textValue = typedArray.getString(R.styleable.BaseTextInput_text)
        val textAlign = typedArray.getInt(R.styleable.BaseTextInput_align, 0)
        val textDesc = typedArray.getString(R.styleable.BaseTextInput_description)

        typedArray.recycle()
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.base_text_input_layout, this, true)

        val parent = getChildAt(0) as LinearLayout
        inputLayout = parent.getChildAt(0) as TextInputLayout
        inputLayout.hint = textHint
        inputLayout.editText?.setText(textValue)
        descTextView = parent.getChildAt(1) as TextView

        if (!textDesc.isNullOrEmpty()) {
            descTextView.visibility = View.VISIBLE
            descTextView.text = textDesc
        } else {
            descTextView.visibility = View.GONE
        }

        when (textAlign) {
            1 -> {
                inputLayout.editText?.textAlignment = View.TEXT_ALIGNMENT_CENTER
                descTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
            }
            2 -> {
                inputLayout.editText?.textAlignment = View.TEXT_ALIGNMENT_VIEW_END
                descTextView.textAlignment = View.TEXT_ALIGNMENT_VIEW_END
            }
            else -> {
                inputLayout.editText?.textAlignment = View.TEXT_ALIGNMENT_VIEW_START
                descTextView.textAlignment = View.TEXT_ALIGNMENT_VIEW_START
            }
        }
    }

    fun getTextValue() : String? {
        return inputLayout.editText?.text?.toString()
    }

    fun setTextValue(value: String?) {
        inputLayout.editText?.setText(value)
    }

    fun setState(enable: Boolean) {
        viewState = enable
        inputLayout.editText?.isEnabled = enable
        inputLayout.editText?.isClickable = enable
    }

    fun getState() : Boolean {
        return viewState
    }

    override fun onSaveInstanceState(): Parcelable {
        val bundle = Bundle()
        bundle.putString(TEXT_VALUE, getTextValue())
        bundle.putBoolean(INPUT_STATE, getState())
        bundle.putParcelable(SUPER_BUNDLE, super.onSaveInstanceState())
        return bundle
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        var stateBundle = state
        if (stateBundle is Bundle) {
            val bundle = stateBundle
            setTextValue(bundle.getString(TEXT_VALUE))
            setState(bundle.getBoolean(INPUT_STATE))
            stateBundle = bundle.getParcelable(SUPER_BUNDLE)
        }
        super.onRestoreInstanceState(stateBundle)
    }
}