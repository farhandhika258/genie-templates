package my.net.xtend.genietreeandroid

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val layoutManager = LinearLayoutManager(this)
        val adapter = MainActivityAdapter(resources.getStringArray(R.array.items))
        val recyclerView = findViewById<RecyclerView>(R.id.item_list_sample).apply {
            this.layoutManager = layoutManager
            this.addItemDecoration(DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL))
            this.adapter = adapter
        }
    }
}
